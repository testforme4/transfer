FROM dutchcoders/transfer.sh:latest

ENV PORT=3000
ENV S3_NO_MULTIPART='false'

EXPOSE 3000

ENTRYPOINT ["/go/bin/transfersh", \
            "--listener", \
            ":${PORT}", \
            "--http-auth-user=${HTTP_AUTH_USER}", \
            "--http-auth-pass=${HTTP_AUTH_PASS}", \
            "--ip-whitelist=${IP_WHITELIST}", \
            "--ip-blacklist=${IP_BLACKLIST}", \
            "--web-path=${WEB_PATH}", \
            "--proxy-path=${PROXY_PATH}", \
            "--proxy-port=${PROXY_PORT}", \
            "--email-contact=${EMAIL_CONTACT}", \
            "--ga-key=${GA_KEY}", \
            "--provider=${PORVIDER}", \
            "--uservoice-key=${USERVOICE_KEY}", \
            "--aws-access-key=${AWS_ACCESS_KEY}", \
            "--aws-secret-key=${AWS_SECRET_KEY}", \
            "--bucket=${BUCKET}", \
            "--s3-endpoint=${S3_ENDPOINT}", \
            "--s3-region=${S3_REGION}", \
            "--storj-access=${STORJ_ACCESS}", \
            "--storj-bucket=${STORJ_BUCKET}", \
            "--basedir=${BASEDIR}", \
            "--gdrive-client-json-filepath=${GDRIVE_CLIENT_JSON_FILEPATH}", \
            "--gdrive-local-config-path=${GDRIVE_LOCAL_CONFIG_PATH}", \
            "--cors-domains=${CORS_DOMAINS}", \
            "--clamav-host=${CLAMAV_HOST}"]